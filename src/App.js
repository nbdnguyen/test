import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <p>
          Welcome to the first test page, and because of that:
        </p>
        <p>Hello, World!</p>
      </header>
    </div>
  );
}

export default App;
